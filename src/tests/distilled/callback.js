var Distilled = require('Distilled');
var show = require('../helpers/show.js');
var performance = require('perf_hooks').performance;

/** @start .section Reporter */

/** If at any point you think of an experiment you'd like to try out, all of the code
 * examples below are editable. */

/** Every instance of Distilled you create can be passed a custom reporter function.
 * This allows you to override Distilled's default test reporter, which controls
 * how test results are logged and displayed. */

/*`` .type source */
show('The callback', function (console) {
    var suite = new Distilled(function (test) {
        console.log('Callback is passed the finished test/suite as a parameter:', test === suite);
        console.log('`this` is set to the finished test/suite:', this === suite);
    });
});
/*``*/

/** Child tests are resolved before parent tests. This allows you to bubble properties and
 * failures up the test chain.
 *
 * As an example, you could reverse the behavior of Distilled's built-in test
 * reporter, which displays tests as passed even if they have a child test that
 * failed. Because reporters know at display time whether or not child tests
 * passed, this is a purely cosmetic choice. */

/*`` .type source */
show('Callback on multiple/chained tests', function (console) {
    var calls = [];
    var suite = new Distilled(function (test) {
        calls.push(test.label);

        if (test === suite) {
            console.log('All tests/suites called in the correct order:', '_a-_b-a-b-A-B-' === calls.join('-'));
        }
    });

    suite.test('A').test('a').test('_a');
    suite.test('B').test('b').test('_b');
});
/*``*/

/** When child tests resolve, a number of properties are attached to them that allow you to
 * to display information about how the test ran. */

/*`` .type source */
show('Test status within callbacks', function (console) {
    var suite = new Distilled(function (test) {
        if (test === passed) {
            console.log('Labels set correctly for passed tests:', test.label === 'passed');
            console.log('`status` is set to `STATUS.PASSED`:', test.status === Distilled.STATUS.PASSED);
            console.log('there is no error:', test.error == null);
        }

        if (test === failed) {
            console.log('Labels set correctly for failed tests:', test.label === 'failed');
            console.log('`status is set to `STATUS.FAILED`:', test.status === Distilled.STATUS.FAILED);
            console.log('Error is set:', test.error === error);
        }
    });

    var passed = suite.test('passed', true);
    var error = new Error('error');
    var failed = suite.test('failed', Promise.reject(error));
});
/*``*/

/** One of Distilled's selling points is that children of failed tests will be
 * ignored and not executed. If a test is already attached before a parent
 * fails, then that test's status will be set to "skipped". */

/*`` .type source */
show('Skipped tests are logged (if already attached)', function (console) {
    var suite = new Distilled(function (test) {
        if (test === a) {
            console.log('Status is set to `STATUS.SKIPPED`:', test.status === Distilled.STATUS.SKIPPED);
        }
    });

    var A = suite.test('A', false),
        a = A.test('a');
});
/*``*/

/** Remember from the very first code sample above that the test passed into the
 * reporter function is literally the same test that you ran before. This means
 * that if you want to attach additional properties, you can. */

/*`` .type source */
show('Extending test statuses', function (console) {

    // Distilled can be extended just like any other class.
    var ExtendedDistilled = function () {
        Distilled.apply(this, arguments);
    };

    // Distilled respects the constructor property when chaining
    // and creating child tests.
    ExtendedDistilled.prototype = Object.create(Distilled.prototype);
    ExtendedDistilled.prototype.constructor = ExtendedDistilled;

    //See the ``assert`` documentation for more info on what's happening here.
    ExtendedDistilled.prototype.assert = function (assertion) {
        var that = this;
        var start = performance.now();

        return Distilled.prototype.assert.call(this, assertion)
            .then(function () {
                that.time = performance.now() - start;
            }, function (err) {
                that.time = performance.now() - start;
                return Promise.reject(err);
            });
    };

    var suite = new ExtendedDistilled(function (test) {
        if (test.label === 'timeout') {
            console.log('Test time was attached', test.time);
            console.log('Timeout test took <500 milliseconds to complete: ', test.time < 500);
        }
    });

    suite.test('timeout', new Promise(function (res) {
        setTimeout(function () { res(); }, 300);
    }));
});
/*``*/
