var Distilled = require('Distilled');
var show = require('../helpers/show.js');

/** @start .section ``then`` */

/** If at any point you think of an experiment you'd like to try out, all of the
 * code examples below are editable. */

/** ``then`` is a power-user feature that allows you to execute logic after
 * a test has finished running. ``then`` is most useful if you are extending
 * Distilled or writing your own project-specific framework and merely using
 * Distilled as a base. */

/*`` .type source */
show('The `then` method: ', function (console) {
    var suite = new Distilled(function () {});

    console.log('Tests/suites expose a `then` method: ', typeof suite.then === 'function');
    console.log('Calling `then` returns the same test/suite you called it on: ', suite === suite.then());

    var test = suite.test('', true);
    test.then(function () {
        console.log('`then` is called once a test finishes.');
    });

    test.then(function () {
        console.log('You can call `then` multiple times on the same test.');

        this.then(function () {
            console.log('`then` calls can even be nested inside themselves.');
        });
    });
});
/*``*/

/** One of the major benefits of leveraging ``then`` over using just ``test``,
 * ``assert``, or even a custom reporter to extend Distilled is that ``then`` is
 * guaranteed to execute no matter what, and is guaranteed to execute only once
 * the entire suite you call it on has finished running.
 */

/*`` .type source */
show('`then` will *always* fire off: ', function (console) {
    var suite = new Distilled(function () {});

    suite.test('', true).then(function () {
        console.log('It will fire if tests pass.');
    });

    suite.test('', false).then(function () {
        console.log('It will fire if tests fail.');
    });

    suite.test('', false).test('').then(function () {
        console.log('It will even fire for children that are skipped.');
    });
});
/*``*/

/*`` .type source */
show('`then` waits for an entire suite to finish: ', function (console) {
    var suite = new Distilled(function () {});
    var parent = suite.test('');

    parent.then(function () {
        var childrenRan = a.status === Distilled.STATUS.PASSED &&
                b.status === Distilled.STATUS.PASSED;

        console.log('Any child tests will have already resolved: ', childrenRan);
    });

    var a = parent.test('a');
    var b = parent.test('b');
});
/*``*/

/** ``then`` resolves using native Promise rules. If you're familiar with how
 * async code works, you already know how ``then`` works. */

/*`` .type source */
show('`then` calls resolve in the same order as native promises: ', function (console) {
    var suite = new Distilled(function () {});

    var calls = [];
    suite.test('A').then(function () {
        calls.push('A');
    });

    suite.test('B').then(function () {
        calls.push('B');
    });

    suite.test('C').then(function () {
        calls.push('C');
    });

    suite.then(function () {
        console.log('A-B-C', calls.join('-') === 'A-B-C');
    });
});
/*``*/

/*`` .type source */
show('Understanding promise resolution allows for advanced timing', function (console) {
    var suite = new Distilled(function () {});

    var calls = [];

    var A = suite.test('A').then(function () {
        calls.push('A');

        this.then(function () {
            calls.push('a');
        });
    });

    suite.test('B').then(function () {
        calls.push('B');
    }).then(function () {
        calls.push('b');
    });

    A.then(function () {
        calls.push('_a');
    });

    suite.then(function () {
        console.log('A-_a-B-b-a', calls.join('-') === 'A-_a-B-b-a');
    });
});
/*``*/

/** Code samples in this section demonstrate bug fixes. They're not particularly
 * relevant to the documentation, but they help make sure that Distilled never
 * has any regressions. */

/*`` .type source */
show('Bug #17: `attachPostResolve` unset', function (console) {
    var suite = new Distilled(function () {});
    var child = suite.test('', true);

    child.then(function () {

        try {
            var postResolveTest = child.test('', true);
        } catch (err) {
            console.log('Adding a new test post-resolution fails.');
            console.log('Test fails with the correct message: ', err.message === suite.MESSAGES.attachPostResolve());
        }
    });
});
/*``*/

/*`` .type source */
show('Bug #17: `attachPostResolve` set', function (console) {
    var suite = new Distilled(function () {}, { attachPostResolve: true });
    var child = suite.test('', true);

    child.then(function () {
        var postResolveTest = child.test('', true);

        postResolveTest.then(function () {
            console.log('`attachPostResolve` allows this behavior: ', postResolveTest.status === Distilled.STATUS.PASSED);
        });
    });
});
/*``*/
