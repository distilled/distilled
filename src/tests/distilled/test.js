var Distilled = require('Distilled');
var show = require('../helpers/show.js');

/** @start .section ``test`` */

/** If at any point you think of an experiment you'd like to try out, all of the
 * code examples below are editable. */

/*`` .type source */
show('The `test` method', function (console) {
    var suite = new Distilled(function () {});

    console.log('Tests/suites expose a `test` method: ', typeof suite.test === 'function');

    var test = suite.test();
    console.log('Calling `test` returns a new test/suite of the same type: ', Object.getPrototypeOf(test) === Object.getPrototypeOf(suite));
});
/*``*/

/** You can learn more about ``assert`` in another section of the documentation.
 * Basically, ``assert`` is the main assertion engine that ``test`` uses
 * to know whether or not a test has passed.
 *
 .link assertions */

/*`` .type source */
show('`test` is a wrapper around `assert`', function (console) {
    function Extension () {
        Distilled.apply(this, arguments);
    }

    Extension.prototype = Object.create(Distilled.prototype);
    Extension.prototype.constructor = Extension;

    var assertion = function () {};
    Extension.prototype.assert = function (claim) {
        console.log('tests are passed into the child assert: ', claim === assertion);
        return Distilled.prototype.assert.apply(suite, arguments);
    };


    var suite = new Extension(function () {});
    suite.test('', assertion);
});
/*``*/

/** Tests are resolved using native Promise rules. If you're familiar with
 * how async code works, you already know how test chaining works. */

/*`` .type source */
show('Child tests are chained off of their parents: ', function (console) {
    var suite = new Distilled(function () {});
    var assertions = [];

    suite.test('A', function () {
        assertions.push('A');
    }).test('a', function () {
        assertions.push('a');
        console.log('Parent tests are resolved before child assertions are called: ', this.parent.status === Distilled.STATUS.PASSED);
    });

    suite.test('B', function () {
        assertions.push('B');
    });

    console.log('Tests are marked as unresolved before they are called: ', suite.status === Distilled.STATUS.PENDING);
    suite.then(function () {
        console.log('Tests are called in the correct order: ', assertions.join('-') === 'A-B-a');
    });
});
/*``*/

/** Failed tests don't call any of their children. You can use this to make your
 * test suite "fail fast". For example, if a database connection fails, there's
 * no point in running tests that rely on that connection, and there's no point
 * in cluttering up your test output with those extra failures.
 *
 * Eliminating superfluous tests will make it easier to see the root cause of
 * that particular failure. */

/*`` .type source */
show('Children of failed tests are not called: ', function (console) {
    var suite = new Distilled(function () {});
    var assertions = [];

    var A = suite.test('A', function () {
        assertions.push('A');
        return false;
    });

    var a = A.test('a', function () {
        assertions.push('a');
    });

    suite.then(function () {
        console.log('Failed tests are marked as failed: ', A.status === Distilled.STATUS.FAILED);
        console.log('Children of failed tests have their assertions skipped: ', 'A' === assertions.join('-'));
        console.log('Children of failed tests are marked as skipped: ', a.status === Distilled.STATUS.SKIPPED);
    });
});
/*``*/

/*`` .type source */
show('Child tests are passed into assertions: ', function (console) {
    var suite = new Distilled(function () {});

    var child = suite.test('', function (param) {
        console.log('Child is passed as a parameters: ', param === child);
        console.log('Child is set to `this`: ', this === child);
    });
});
/*``*/

/** You can traverse your entire test tree at any point, even while a suite is
 * running. This is rarely necessary, but can be occasionally used in powerful
 * ways to build very creative extensions or test reporters. */

/*`` .type source */
show('Children/parents are attached to each other', function (console) {
    var suite = new Distilled(function () {});

    suite.test('', function () {
        console.log('Child has a reference to the parent: ', this.parent === suite);
        console.log('Parent has a reference to the child: ', suite.children[0] === this);
    });

    suite.test('', function () {
        console.log('Multiple children can be attached: ', suite.children[1] === this);
    });
});
/*``*/

/** Of course, Distilled is designed to be highly extensible. */

/*`` .type source */
show('Extending `test`', function (console) {
    function Extension () {
        Distilled.apply(this, arguments);
    }

    Extension.prototype = Object.create(Distilled.prototype);
    Extension.prototype.constructor = Extension;

    var suite = new Extension(function () {});
    var test = suite.test();
    console.log('correct prototype is attached to new suites when calling `test`: ', Object.getPrototypeOf(test) === Object.getPrototypeOf(suite));
});
/*``*/

/** .section Bugs */

/** Code samples in this section demonstrate bug fixes. They're not particularly
 * relevant to the documentation, but they help make sure that Distilled never
 * has any regressions. */

/*`` .type source */
show('Bug check: `timeout` option is passed into child tests', function (console) {
    var suite = new Distilled(function () {}, { timeout: 50 });

    var rejected = suite.test('', true).test('', new Promise(function () {}));

    rejected.then(function () {
        console.log('An unresolved promise will time out: ', rejected.status === Distilled.STATUS.FAILED);
    });
});
/*``*/
