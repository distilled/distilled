var Distilled = require('Distilled');
var show = require('../helpers/show.js');

/** .section ES7+ */

/** Distilled's code examples use traditional "old-style" code. This makes code
 * simpler for newer developers to read. Distilled is of course compatible with
 * newer Javascript features like arrow functions, classes, and async
 * methods. */

/*``*/
show('Arrow functions', function (console) {
    const suite = new Distilled();

    suite.test('foo', ()=>()=>()=>true)
        .then((test)=>console.log('Arrow functions can be used for passing tests: ', test.status === Distilled.STATUS.PASSED));
    suite.test('bar', ()=>()=>()=>false)
        .then((test)=>console.log('Arrow functions can be used for failing tests: ', test.status === Distilled.STATUS.FAILED));
});
/*``*/

/*``*/
show('`async`', function (console) {
    const suite = new Distilled();

    let order = '';
    suite.test('foo', async ()=>{
        await new Promise(res => {
            setTimeout(()=>{
                order += '1';
                res();
            }, 100);
        });

        order += '2';

    }).then(()=>{
        console.log('Normal async rules apply: ', order === '12');
    });
});
/*``*/

/*``*/
show('Class-based extensions', function (console) {
    class ExtendedDistilled extends Distilled {
        constructor (reporter, options) {
            super(reporter, options);
        }

        assert () {
            return super.assert(()=>{ throw new Error('I got called!'); });
        }
    }

    const suite = new ExtendedDistilled((test) => {
        if (test.label === 'foo') {
            console.log('`assert` failed the test: ', test.status === ExtendedDistilled.STATUS.FAILED);
            console.log('`assert set the error`: ', test.error.message === 'I got called!');
        }
    });

    suite.test('foo', true);
    console.log('Static methods are attached to extended classes: ', ExtendedDistilled.STATUS === Distilled.STATUS);
});
/*``*/
