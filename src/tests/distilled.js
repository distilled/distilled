console.log('Distilled self-test: ');
require('./distilled/overview.js');
require('./distilled/test.js');
require('./distilled/then.js');
require('./distilled/callback.js');
require('./distilled/assertions.js');
