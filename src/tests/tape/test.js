/*
 * BASIC TEST BEHAVIOR
 *------------------------------------------------------
 *
 * Tests can be infinitely nested and chained.
 * - structure can be accessed through `parent` and `children`
 * - children of failed tests are not called/resolved
 */
var tape = require('tape');
var Distilled = require('Distilled');

tape('Tests/suites expose a `test` method', function (t) {
    t.plan(3);

    var suite = new Distilled(function () {});

    t.equal(typeof suite.test, 'function', '`test` function is exposed');

    //Use OK instead of `equal` because tape tries to incorrectly access prototype properties during the test
    t.ok(Object.getPrototypeOf(suite) === Object.getPrototypeOf(suite.test('A')),
         'Calling `test` returns a new test/suite of the same type');

    //Also works when extending prototype
    function Extension () {
        Distilled.apply(this, arguments);
    }
    Extension.prototype = Object.create(Distilled.prototype);
    Extension.prototype.constructor = Extension;

    var _extension = new Extension(function () {});
    t.ok(Object.getPrototypeOf(_extension) === Object.getPrototypeOf(_extension.test()),
         'Calling `test` on an extended suite returns a new test/suite of the same type');
});

tape('Child tests/suites are chained off of their parents', function (t) {
    t.plan(4);

    var calls = [];
    var tests = [];

    var suite = new Distilled(function () {});
    var child = suite.test('A', function () {
        calls.push('A');
    }).test('a', function (test) {
        calls.push('a');

        /*
         * Promise resolution order means that this should resolve last.
         * - promises that are chained will resolve after promises that are added synchronously.
         */
        t.deepEqual(calls, ['A', 'B', 'a'], 'Child tests are resolved in the correct order');

        t.equals(test.parent.label, 'A', '`parent` points to parent test');
        t.equals(test.parent.status, Distilled.STATUS.PASSED, 'Child is resolved after parent resolves');
    });

    child.test('_a', function (test) {
        t.deepEqual(child.children[0], test, 'Children are added to `children` array on parent test/suite');
    });

    suite.test('B', function () {
        calls.push('B');
    });
});

tape('Ignore children of failed tests', function (t) {
    t.plan(1);

    var calls = [];

    var suite = new Distilled(function () {});
    suite.test('A', false).test('a', function () {
        calls.push('a');
    });

    /*
     * Once again exploiting promise resolution order to guarantee that the above test has an opportunity to fail.
     */
    suite.test('B').test('b').test('_b', function () {
        t.equal(calls.length, 0, 'Children of failed tests are never resolved');
    });

})

