/*
 * BASIC TEST RESOLUTION
 *----------------------------------------------------------
 *
 * This checks that logic for tests passing/failing works correctly
 * It also does shallow tests for recursion
 *
 * Ways to resolve a test include:
 * - passing nothing
 * - passing a (failed) promise
 * - passing false
 *
 * If a function or a resolved promise are passed:
 * - the returned/resolved result is recursively evaluated with the above rules
 */
var tape = require('tape');
var Distilled = require('Distilled');

tape('accept `null`', function (t) {
    t.plan(2);

    var suite = new Distilled(function (test) {

        if (test.label === 'A' || test.label === 'B') {
            t.equal(test.status, Distilled.STATUS.PASSED, 'Passing `null` or `undefined` causes the test to pass');
        }
    });

    suite.test('A', null);
    suite.test('B', undefined);
});

tape('accept Boolean`', function (t) {
    t.plan(2);

    var suite = new Distilled(function (test) {
        if (test.label === 'A') {
            t.equal(test.status, Distilled.STATUS.PASSED, 'Passing `true` causes the test to pass');
        }

        if (test.label === 'B') {
            t.equal(test.status, Distilled.STATUS.FAILED, 'Passing `false` causes the test to fail');
        }
    });

    suite.test('A', true);
    suite.test('B', false);
});

tape('accept Promise', function (t) {
    t.plan(3);

    var suite = new Distilled(function (test) {
        if (test.label === 'A') {
            t.equal(test.status, Distilled.STATUS.PASSED, 'Passing Promise (resolved) causes the test to pass');
        }

        if (test.label === 'B') {
            t.equal(test.status, Distilled.STATUS.FAILED, 'Passing Promise (rejected) causes the test to fail');
            t.equal(test.error, 'error', '`test.error` is set to the rejected promise\'s error');
        }
    });

    suite.test('A', Promise.resolve());
    suite.test('B', Promise.reject('error'));
});

tape('accept Promise: advanced', function (t) {
    t.plan(4);

    var suite = new Distilled(function (test) {
        if (test.label === 'A') {
            t.equal(test.status, Distilled.STATUS.FAILED, 'A promise resolving to false causes the test to fail');
        }
    });

    suite.test('A', Promise.resolve(false));
    var B = suite.test('B', Promise.resolve(function (test) {
        t.pass('A promise resolving to a function calls that function');

        t.equal(test, B, 'The resulting test/suite is passed into the function');
        t.equal(test, this, '`this` is set to the resulting test/suite');
    }));
});

tape('accept Function', function (t) {
    t.plan(7);

    var suite = new Distilled(function (test) {
        switch (test.label) {
        case 'A':
            t.equal(test.status, Distilled.STATUS.PASSED, 'returning `undefined` from a function causes test to pass');
            break;
        case 'B':
            t.equal(test.status, Distilled.STATUS.FAILED, 'returning `false` from a function causes test to fail');
            break;
        case 'C':
            t.equal(test.status, Distilled.STATUS.FAILED, 'throwing exception from function causes test to fail');
            t.equal(test.error, 'error', '`test.error` is set to the thrown exception');
        }
    });

    var A = suite.test('A', function (test) {
        t.pass('passing a function calls the function');

        t.equal(test, A, 'The resulting test/suite is passed into the function');
        t.equal(test, this, '`this` is set to the resulting test/suite');
    });
    suite.test('B', function () { return false; });
    suite.test('C', function () { throw 'error'; });
});
