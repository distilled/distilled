/*
 * CALLBACK TESTS
 *-----------------------------------------------------------
 *
 * Whenever a test resolves, a callback is called:
 * - this is primarily a place to do reporting and logging
 * - it can also be adapted into a harness runner
 * - it can also be used to extend Distilled and add new features
 *
 * You don't need to understand how Promises work to use Distilled:
 * - but, if you do understand Promises, that will probably make you better
 * - tests are executed as promises, and when they resolve, the callback is called
 */
var Distilled = require('Distilled');
var tape = require('tape');

tape('Basic callback', function (t) {
    t.plan(3);

    var suite = new Distilled(function (test) {
        t.pass('Callback is called once a suite has finished.');
        t.equal(test, suite, 'Callback is passed the resulting test/suite as a parameter');
        t.equal(this, suite, '`this` is set to the resulting test/suite');
    });
});

tape('Multiple tests', function (t) {
    t.plan(2);

    var calls = [];
    var tests = [];

    var suite = new Distilled(function (test) {
        calls.push(test);

        if (calls.length === 3) {
            t.pass('Callback is called for each test');
            t.deepEqual(calls, tests, 'Correct test/suite passed into each callback');
        }
    });

    tests.push(suite.test('A'));
    tests.push(suite.test('B'));
    tests.push(suite);
});

tape('Nested tests', function (t) {
    t.plan(2);

    var calls = [];
    var tests = [];

    var suite = new Distilled(function (test) {
        calls.push(test);

        if (calls.length === 4) {
            t.pass('Callback is called for each test');
            t.deepEqual(calls, tests, 'Correct test/suite passed into each callback');
        }
    });

    var A = suite.test('A');
    var B = A.test('B');
    var C = B.test('C');

    tests.push(C);
    tests.push(B);
    tests.push(A);
    tests.push(suite);
});

tape('test statuses are set correctly for passed test', function (t) {
    t.plan(3);

    var pass;
    var suite = new Distilled(function (test) {
        if (test === pass) {
            t.equal(test.label, 'A', '`test.label` is the label originally passed into the test');
            t.equal(test.status, Distilled.STATUS.PASSED, '`test.status` is set to `STATUS.PASSED`');
            t.equal(test.error, null, '`test.error` in `null` for passed test');
        }
    });

    pass = suite.test('A');
});

tape('test statuses are set correctly for failed test', function (t) {
    t.plan(3);

    var fail,
        error = new Error('error');

    var suite = new Distilled(function (test) {
        if (test === fail) {
            t.equal(test.label, 'A', '`test.label` is the label originally passed into the test');
            t.equal(test.status, Distilled.STATUS.FAILED, '`test.status` is set to `STATUS.FAILED`');
            t.equal(test.error, error, '`test.error` is set to the error the test failed with');
        }
    });

    fail = suite.test('A', Promise.reject(error));
});

tape('Skipped children are still called', function (t) {
    t.plan(1);

    var calls = 0;
    var suite = new Distilled(function () {
        ++calls;

        if (this === suite) {
            t.equal(calls, 7, 'callbacks are not called for skipped tests');
        }
    });

    suite.test('A', false).test('a').test('_a');
    suite.test('B').test('b').test('_b');
});
