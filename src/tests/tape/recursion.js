/*
 * RESOLUTION RECURSION
 *-----------------------------------------------------
 * Short tests to provide some evidence that resolution recursion is actually infinite
 */
var tape = require('tape');
var Distilled = require('Distilled');

tape('Recursively resolving function results', function (t) {
    t.plan(2);

    var suite = new Distilled(function (test) {
        if (test.label === 'A') {
            t.equal(test.status, Distilled.STATUS.PASSED, 'nested functions eventually pass correctly');
        }

        if (test.label === 'B') {
            t.equal(test.status, Distilled.STATUS.FAILED, 'nested functions eventually fail correctly');
        }
    });

    suite.test('A', function () {
        return function () {
            return function () {
                return function () {
                    return function () {
                        return true;
                    };
                };
            };
        };
    });

    suite.test('B', function () {
        return function () {
            return function () {
                return function () {
                    return function () {
                        return false;
                    };
                };
            };
        };
    });
});

tape('Recursively resolving promise results', function (t) {
    t.plan(2);

    var suite = new Distilled(function (test) {
        if (test.label === 'A') {
            t.equal(test.status, Distilled.STATUS.PASSED, 'nested promises eventually pass correctly');
        }

        if (test.label === 'B') {
            t.equal(test.status, Distilled.STATUS.FAILED, 'nested promises eventually fail correctly');
        }
    });

    suite.test('A', Promise.resolve(
        Promise.resolve(
            Promise.resolve(
                Promise.resolve(
                    Promise.resolve(true)
                )
            )
        )
    ));

    suite.test('B', Promise.resolve(
        Promise.resolve(
            Promise.resolve(
                Promise.resolve(
                    Promise.resolve(false)
                )
            )
        )
    ));
});

tape('Recursively resolving "weird" results', function (t) {
    t.plan(2);

    var suite = new Distilled(function (test) {
        if (test.label === 'A') {
            t.equal(test.status, Distilled.STATUS.PASSED, 'nested (mixed) promises/functions eventually pass correctly');
        }

        if (test.label === 'B') {
            t.equal(test.status, Distilled.STATUS.FAILED, 'nested (mixed) promises/functions eventually fail correctly');
        }
    });

    suite.test('A', function () {
        return Promise.resolve(function () {
            return Promise.resolve(function () {
                return true;
            });
        });
    });

    suite.test('B', function () {
        return Promise.resolve(function () {
            return Promise.resolve(function () {
                return false;
            });
        });
    });
});

tape('Exceptions from functions within promises (#9)', function (t) {
    t.plan(1);

    var suite = new Distilled(function (test) {
        if (test.label === 'A') {
            t.equal(test.status, Distilled.STATUS.FAILED, 'Exceptions from recursively called functions are caught correctly');
        }
    });

    suite.test('A', function () {
        return Promise.resolve(function () {
            throw 'error';
        });
    });
});
