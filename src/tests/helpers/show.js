var Distilled = require('Distilled');
var reporter = require('distilled-reporters-checklist');

function timer(timeout, rejection) {
    var result = {};
    result.promise = new Promise(function (res, rej) {
        var wait = setTimeout(function () {
            rej(rejection());
        }, timeout);

        result.resolve = function () {
            clearTimeout(wait);
            res.apply(null, arguments);
        };
    });

    return result;
}

//--------------ACTUAL SETUP-----------------

var timeout = 2000;
var suite = new Distilled(function () {
    console.log = console.logActual; /* Reattach console for our reporter */
    reporter.apply(this, arguments);
    console.log = function () {}; /* Then get rid of it again. */
}, {
    timeout: timeout
});

function show (topic, explanation) {
    var source = explanation.toString();

    //write assertions to an object so we can figure out which have run and which haven't.
    var assertions = {};
    var assertionsRun = 0;
    (source.match(/console\.log\(.*\)/g) || [])
        .forEach(function (assertion) {

            /* This could easily break, but it's not really a problem, my use case is *very* narrow. */
            assertion = assertion.match(/['"](.*)['"],/) || assertion.match(/['"](.*)['"]\)/);

            assertion = assertion[1].replace('\\', '');
            assertions[assertion] = null;
        });

    var def = timer(timeout - 500, function () {
        var messages = [];
        Object.keys(assertions).forEach(function (key) {
            if (!assertions[key]) {
                messages.push(key);
            }
        });

        return new Error('The following tests were not run: \n' + messages.join('\n'));
    });

    var test; //Parent context
    var fakeConsole = {
        log : function (message, assert) {
            assert = assert || arguments.length === 1; /* Allow undefined to count as true. */

            test.test(message, assert);

            assertions[message] = true;
            ++assertionsRun;

            if (assertionsRun === Object.keys(assertions).length) {
                def.resolve();
            }
        },
        info : console.info.bind(console) /* For actual debug purposes. */
    };

    /* Don't log anything normal. This will get run multiple times, so we have a
     * quick extra safety check to make sure we don't overwrite `console.log` a
     * second time with the same mock. */
    console.logActual = console.logActual || console.log;
    console.log = function () {};

    /* Run test */
    test = suite.test(topic, function () {
        if (Object.keys(assertions).length === 0) {
            def.resolve();
        }

        return def.promise;
    });

    explanation(fakeConsole);
}

module.exports = show;
